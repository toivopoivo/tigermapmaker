using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ToivonRandomUtils;

namespace TigerMapMakerSpace
{
	public class TigerMapMaker:MonoBehaviour
	{
		public TigerMapMakerAssets assets;
		public int mapSize = 10;
		[HideInInspector]
		public int lastMapSize = -1;
		public GameObject thrashParent;

		public bool debugBreaking = false;
		public Map map;
		public List<VisualisationType> visualizations = new List<VisualisationType> ();

		public enum VisualisationType
		{
			Outling,
			Coasting
		}

		[System.Serializable]
		public class Map: IEnumerable<KeyValuePair<IntVector2,MapCell>>
		{
			[SerializeField]
			List<MapCell> mapCells = new List<MapCell> ();
			[SerializeField]
			int rowLenght;

			int IntVector2ToCellIndex (IntVector2 intVector2)
			{
				return (intVector2.y * rowLenght) + intVector2.x;
			}

			public MapCell TryGetCell (IntVector2 intVector2)
			{
				MapCell mapCell;
				if (intVector2.x < 0 || intVector2.y < 0 || intVector2.x >= rowLenght || intVector2.y >= rowLenght) {
					return null;
				}

				try {
					mapCell = mapCells [IntVector2ToCellIndex (intVector2)];
				} catch (System.Exception ex) {
					throw ex;
				}
				return mapCell;
			}

			public void SetCell (IntVector2 intVector2, MapCell mapCell)
			{
				mapCells [IntVector2ToCellIndex (intVector2)] = mapCell;
			}

			public void SetSizeTo (int newRowLength, TigerMapMakerAssets assets)
			{
				for (int i = 0; i < newRowLength; i++) {
					for (int j = 0; j < newRowLength; j++) {
						if (rowLenght <= i || rowLenght <= j) {
							var mapCell = new MapCell ();
							mapCell.terrainType = assets.terrainTypes.First ();
							mapCells.Add (mapCell);
						}
					}
				}
				rowLenght = newRowLength;
			}

			#region IEnumerable implementation

			public IEnumerator<KeyValuePair<IntVector2, MapCell>>  GetEnumerator ()
			{
				for (int i = 0; i < rowLenght; i++) {
					for (int j = 0; j < rowLenght; j++) {
						var gridPos = new IntVector2 (i, j);
						yield return new KeyValuePair<IntVector2,MapCell> (gridPos, TryGetCell (gridPos));
					}
				}
			}

			#endregion

			#region IEnumerable implementation

			IEnumerator IEnumerable.GetEnumerator ()
			{
				yield return GetEnumerator ();
			}

			#endregion
		}

		public List<TransitionDirectionOffset> transDirOffsets = new List<TransitionDirectionOffset> ();

		[System.Serializable]
		public class TransitionDirectionOffset
		{
			public IntVector2.DirectionType directionType;
			public Vector3 positionOffset;
			public Vector3 rotationOffset;

			public	void Apply (GameObject target)
			{
				target.transform.position += positionOffset; 
				target.transform.rotation = Quaternion.Euler (rotationOffset);

			}
		}



		[ToivonRandomUtils.InspectorButton]
		public void ReDrawMapInEditor ()
		{
			currentDrawing = Drawing ();
			if (!Application.isPlaying) {
				while (currentDrawing.MoveNext ()) {
				}
			}
		}

		void Start ()
		{
			var drawing = Drawing ();
			while (drawing.MoveNext ()) {
			}
		}

		void Update ()
		{
			if (currentDrawing != null) {
				currentDrawing.MoveNext ();
			}


		}

		[ToivonRandomUtils.InspectorButton]
		void VizOutlines ()
		{
			StartCoroutine (QuickDrawOutlines ());
		}

		IEnumerator QuickDrawOutlines ()
		{
			if (Application.isPlaying) {
				islands.ForEach (x => {
					var color = new Color (Random.value, Random.value, Random.value, 1f);
					x.outline.ForEach (y => TryVisualizeSomethingWithCube ("island", color, y.Vector3XZ));
				});
				yield return null;
				ClearViz ();
			} 
		}


		IEnumerator currentDrawing;

		GameObject InstantiateMapPrefab (IntVector2 gridPos, GameObject prefab)
		{
			var instantiatedPrefab = UnityEditor.PrefabUtility.InstantiatePrefab (prefab) as GameObject;
			instantiatedPrefab.gameObject.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
			instantiatedPrefab.transform.SetParent (mapParent.transform);
			instantiatedPrefab.transform.position = gridPos.Vector3XZ;
			var mapMakerCellComponent = instantiatedPrefab.AddComponent<MapMakerCellComponent> ();
			mapMakerCellComponent.tigerMapMaker = this;
			mapMakerCellComponent.cellPosition = gridPos;
			mapMakerCellComponent.copyedCellProperties = map.TryGetCell (gridPos);
			return instantiatedPrefab;
		}

		public class CollectedTransition
		{
			public KeyValuePair<IntVector2, MapCell> neigbour;
			public KeyValuePair<IntVector2, MapCell> current;
			public IntVector2.DirectionType dirType;
			//			public GameObject prefab;
		}

		void InstantiateTransition (IntVector2 gridPos, GameObject prefab, IntVector2.DirectionType dirType)
		{
			var createdTransitionPrefab = InstantiateMapPrefab (gridPos, prefab);
			createdTransitionPrefab.GetComponent<MapMakerCellComponent> ().transitionDirection = dirType;
			TransitionDirectionOffset transitionDirectionOffset;
			try {
				transitionDirectionOffset = transDirOffsets.Single (x => x.directionType == dirType);
			} catch (System.Exception ex) {
				Debug.Log ("no transition direction offsets for direction type" + dirType, createdTransitionPrefab);
				throw ex;
			}
			transitionDirectionOffset.Apply (createdTransitionPrefab);
		}

		[HideInInspector]
		public Transform vizParent;

		void TryVisualizeSomethingWithCube (string str, Color color, Vector3 position)
		{
			if (!Application.isPlaying || disableVizTemp) {
				return;
			}

			if (vizParent == null) {
				vizParent = new GameObject ("viz parent").transform;
			}
			var gameObject2 = GameObject.CreatePrimitive (PrimitiveType.Cube);
			gameObject2.name = str;
			Debug.Log (str, gameObject2);
			gameObject2.GetComponent<MeshRenderer> ().material.color = color;
			gameObject2.transform.SetParent (vizParent);
			gameObject2.transform.position = position;
		}


		public GameObject mapParent;

		public class Island
		{
			public TerrainType terrainType;
			public List<IntVector2> space = new List<IntVector2> ();
			public List<IntVector2> outline = new List<IntVector2> ();
		}

		List<Island> islands;

		IEnumerator Outlining ()
		{
			islands = new List<Island> ();
			List<IntVector2> open = new List<IntVector2> ();


			foreach (var item in map) {
				IntVector2 current = item.Key;
				if (islands.Any (x => x.space.Contains (current))) { 
					TryVisualizeSomethingWithCube ("already mapped", Color.red, current.Vector3XZ);
					yield return null;
					continue;	
				} 
				var currentIsland = new Island ();
				currentIsland.terrainType = item.Value.terrainType;
				islands.Add (currentIsland);

				while (true) {
					currentIsland.space.Add (current);
					TryVisualizeSomethingWithCube ("current", Color.yellow, current.Vector3XZ);
					open.ForEach (x => TryVisualizeSomethingWithCube ("open", Color.white, x.Vector3XZ));
					
					for (int i = 0; i < 8; i++) {
						var neighbourPosition = current + IntVector2.AllNeighbours [i];
						var neighbourCell = map.TryGetCell (neighbourPosition);
						if (neighbourCell == null) {
							continue;
						}
						if (neighbourCell.terrainType != item.Value.terrainType) {
							currentIsland.outline.Add (current);
							continue;
						}
						if (currentIsland.space.Contains (neighbourPosition)) {
							continue;
						}
						if (open.Contains (neighbourPosition))
							continue;
						open.Add (neighbourPosition);
						
					}

					if (!open.Any ()) {
						break;	 
					} else {
						current = open.First ();
						open.RemoveAt (0);
					}
					
					yield return null;
				}

				currentIsland.outline = currentIsland.outline.Distinct ().ToList ();

			}
		}

		/// <summary>
		/// Coasting here means creating coasts. this how i english
		/// </summary>
		IEnumerator Coasting (Island targetIsland)
		{
			List<int> drawnIndexes = new List<int> ();

			for (int i = 0; i < targetIsland.outline.Count; i++) {
				List<IntVector2> withinForeingIsland = new List<IntVector2> ();
				for (int j = 0; j < 4; j++) {
					var mapPos = targetIsland.outline [i] + IntVector2.FixedAxisNeighbours [j];
					if (map.TryGetCell (mapPos).terrainType != targetIsland.terrainType) {
						withinForeingIsland.Add (mapPos);
					}
				}

				IntVector2.DirectionType dir = IntVector2.DirectionType.None;
				if (withinForeingIsland.Count == 1) {
					dir = (withinForeingIsland [0] - targetIsland.outline [i]).GetDirection ();
					InstantiateTransition (targetIsland.outline [i], targetIsland.terrainType.transitionOut, dir);
					drawnIndexes.Add (i);
				} else if (withinForeingIsland.Count == 2) {
					var deltaOne = withinForeingIsland [0] - targetIsland.outline [i];
					var deltaTwo = withinForeingIsland [1] - targetIsland.outline [i];
					dir = (deltaOne + deltaTwo).GetDirection ();
					InstantiateTransition (targetIsland.outline [i], targetIsland.terrainType.bigCorner, dir);
					drawnIndexes.Add (i);
				} else if (withinForeingIsland.Count > 2) {
					var spamGo = GameObject.CreatePrimitive (PrimitiveType.Cube);
					spamGo.transform.name = "too narrow!!!";
					spamGo.transform.SetParent (mapParent.transform);
					spamGo.transform.position = targetIsland.outline [i].Vector3XZ.SameButDifferent (y: 1f);
					Debug.LogError ("too narrow!!!!!!", spamGo);
					if (debugBreaking) Debug.Break ();
				}
				if (Application.isPlaying) Debug.Log ("dir: " + dir);
				yield return null;
			}

			// if all goes well all non drawn cells are "small corners" i.e 
			// only one of the 8 neigbour cells is a DIRTY FOREINGER
			if (targetIsland.terrainType.smallCorner == null) {
				Debug.LogError ("no small corner speciefeid for " + targetIsland.terrainType);
			} else {

				for (int i = 0; i < targetIsland.outline.Count; i++) {
					if (drawnIndexes.Contains (i)) {
						continue;
					}
					var singleForeingerDir = IntVector2.AllNeighbours.Single (x => targetIsland.terrainType != map.TryGetCell (targetIsland.outline [i] + x).terrainType);
					var pos = targetIsland.outline [i];
					var transform2 = InstantiateMapPrefab (pos, targetIsland.terrainType.smallCorner).transform;
					transform2.rotation = Quaternion.LookRotation (singleForeingerDir.Vector3XZ);
					transform2.position = pos.Vector3XZ + new Vector3 (0.5f, 0, 0.5f);
				}
			}

		}


		void ClearViz ()
		{
			if (vizParent != null)
				Destroy (vizParent.gameObject);
		}

		bool disableVizTemp;

		[ContextMenu ("nuke map")]
		void NukeMap ()
		{
			map = new Map ();
		}

		IEnumerator Drawing ()
		{
			if (thrashParent != null) {
				DestroyImmediate (thrashParent);
			}
			map.SetSizeTo (mapSize, assets);
			thrashParent = new GameObject ("MAP MAKER THRASH PARENT");
			UnityEditor.Undo.RegisterCreatedObjectUndo (thrashParent, "new trashparent");
			thrashParent.transform.SetParent (transform);
			thrashParent.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
			mapParent = new GameObject ("map parent");
			mapParent.transform.SetParent (thrashParent.transform);
			Dictionary<IntVector2, int> transitionCounts = new Dictionary<IntVector2, int> ();
			List<CollectedTransition> inTransitions = new List<CollectedTransition> ();
			List<CollectedTransition> outTransitions = new List<CollectedTransition> ();

			/*
			for (int i = 0; i < mapSize; i++) {
				for (int j = 0; j < mapSize; j++) {
					IntVector2 gridPos = new IntVector2 (j, i);
					var currentCell = map.TryGetCell (gridPos);
					for (int k = 0; k < 2; k++) {
						var neighbourPos = gridPos + IntVector2.FixedAxisNeighbours [k];
						var neighbourCell = map.TryGetCell (neighbourPos);
						if (neighbourCell != null && neighbourCell.terrainType != currentCell.terrainType) {
							var directionType = IntVector2.FixedAxisNeighbours [k].GetDirection ();
							inTransitions.Add (
								new CollectedTransition () {
									current = new KeyValuePair<IntVector2, MapCell> (gridPos, currentCell),
									neigbour = new KeyValuePair<IntVector2, MapCell> (neighbourPos, neighbourCell),
//									prefab = assets.terrainTypeTransitions.Single (x => x.IsTransitionForAB (neighbourCell.terrainType, currentCell.terrainType)).prefab,
									dirType = directionType
								}
							);

							var currentTerrainTypeOutTransitionPrefab = currentCell.terrainType.transitionOut;
							if (currentTerrainTypeOutTransitionPrefab != null) {
								outTransitions.Add (new CollectedTransition () {
									current = new KeyValuePair<IntVector2, MapCell> (gridPos, currentCell),
									neigbour = new KeyValuePair<IntVector2, MapCell> (neighbourPos, neighbourCell),
//									prefab = currentTerrainTypeOutTransitionPrefab,
									dirType = directionType
								});
							}
						} 

					}

				}
			}*/



			IEnumerator currentOperation = Outlining ();
			disableVizTemp = !visualizations.Contains (VisualisationType.Outling);
			while (currentOperation.MoveNext ()) {
				if (!disableVizTemp) yield return null;
				ClearViz ();
			}

			disableVizTemp = !visualizations.Contains (VisualisationType.Coasting);

			foreach (var item in islands) {
				currentOperation = Coasting (item);
				while (true) {
					bool complete = false;
					try {
						complete = !currentOperation.MoveNext ();
					} catch (System.Exception ex) {
						ClearViz ();
						Debug.LogError (ex);
						break;
					}

					if (!disableVizTemp) yield return null;
					ClearViz ();
					if (complete) break;
				}
			}


			disableVizTemp = false;

			
			VizOutlines ();
			yield return null;
			ClearViz ();

			/*		var transitionGroups = inTransitions.GroupBy (x => x.neigbour);
			foreach (var item in transitionGroups) {
				var collectedTransition = item.First ();
				var terrainTypeTransition = assets.terrainTypeTransitions.Single (x => x.IsTransitionForAB (collectedTransition.current.Value.terrainType, collectedTransition.neigbour.Value.terrainType));
				if (item.Count () == 1) {
					InstantiateTransition (collectedTransition.current.Key, terrainTypeTransition.prefab, collectedTransition.dirType);
				} else if (item.Count () == 2) {
					
//					InstantiateTransition (  
					var terrainTypeThatThisCornerIsSurroundedByMost = IntVector2.AllNeighbours.Select (x => map.TryGetCell (x + collectedTransition.neigbour.Key)).Where (x => x != null).GroupBy (x => x.terrainType).OrderByDescending (x => x.Count ()).First ().Key;
					if (terrainTypeTransition.transitionTypeA == terrainTypeThatThisCornerIsSurroundedByMost) {
						InstantiateTransition (collectedTransition.current.Key, terrainTypeTransition.cornerBigAPrefab, collectedTransition.dirType);
					} else {
						InstantiateTransition (collectedTransition.current.Key, terrainTypeTransition.cornerBigBPrefab, collectedTransition.dirType);
					}
					Debug.Log ("CORNERUU!!!!!" + item.Aggregate ("", (x, y) => x + " " + y.dirType));
					if (debugBreaking) Debug.Break ();
				} else {
					Debug.Log ("MEGA CORNER??????????????");
					if (debugBreaking) Debug.Break ();
				}
				yield return null;
			}
			*/
			
			
			foreach (var item in map) {
				if (islands.Any (x => x.outline.Contains (item.Key)))
					continue;
				var gameObject2 = InstantiateMapPrefab (item.Key, item.Value.terrainType.prefab);
				yield return null;
			}
		}

		//		void InstantiateOutTransition ()
		//		{
		//			var createdOutTransPrefab = InstantiateMapPrefab (gridPos, currentTerrainTypeOutTransitionPrefab);
		//			transitionDirectionOffset.Apply (createdOutTransPrefab);
		//			terrainOutTransition = true;
		//		}

	
	

	}

	[System.Serializable]
	public class MapCell
	{
		public TerrainType terrainType;
	}
}



