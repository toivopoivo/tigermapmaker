﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;

namespace TigerMapMakerSpace
{
	public class MapMakerCellComponent : MonoBehaviour, IOnValidateSafe
	{
		public IntVector2 cellPosition;
		public TigerMapMaker tigerMapMaker;
		public MapCell copyedCellProperties;


		[Header("debug")]
		public IntVector2.DirectionType transitionDirection = IntVector2.DirectionType.None;
		#region IOnValidateSafe implementation

		public void OnValidateSafe ()
		{
			#if UNITY_EDITOR
			UnityEditor.Undo.RecordObject (tigerMapMaker, "tiger map maker cell change");
			#endif
			tigerMapMaker.map.SetCell (cellPosition, copyedCellProperties);
		}

		#endregion

		[ToivonRandomUtils.InspectorButton]
		void ReDrawMapInEditor ()
		{
			tigerMapMaker.ReDrawMapInEditor ();
		}


	}
}