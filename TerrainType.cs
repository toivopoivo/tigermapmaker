using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TigerMapMakerSpace
{

	[CreateAssetMenu]
	public class TerrainType : ScriptableObject
	{
		public GameObject prefab;
		public GameObject transitionOut;
		[UnityEngine.Serialization.FormerlySerializedAs ("cornerTransitionOut")]
		public GameObject bigCorner;

		public GameObject smallCorner;
	}
	
}