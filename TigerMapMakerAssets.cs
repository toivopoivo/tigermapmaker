﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TigerMapMakerSpace
{
	[CreateAssetMenu]
	public class TigerMapMakerAssets : ScriptableObject
	{
		public List<TerrainType> terrainTypes;

		public List<TerrainTypeTransition> terrainTypeTransitions = new List<TerrainTypeTransition> ();


		[ToivonRandomUtils.InspectorButton]
		void ClearAndGenerateTransitions ()
		{
			#if UNITY_EDITOR
			UnityEditor.Undo.RecordObject (this, "clear generat");
			UnityEditor.EditorUtility.SetDirty (this);


			terrainTypeTransitions.Clear ();

			for (int i = 0; i < terrainTypes.Count; i++) {
				for (int j = i + 1; j < terrainTypes.Count; j++) {
					var createdTrans = ToivonRandomUtils.RandomUtils.CreateAssetObject<TerrainTypeTransition> (string.Format ("{0}_to_{1}", terrainTypes [i].name, terrainTypes [j].name));
					terrainTypeTransitions.Add (createdTrans);
					createdTrans.transitionTypeA = terrainTypes [i];
					createdTrans.transitionTypeB = terrainTypes [j];
				}
			}


			#endif
		}
	}


}