using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;

namespace TigerMapMakerSpace
{

	[CreateAssetMenu]
	public class TerrainTypeTransition : ScriptableObject
	{
		public TerrainType transitionTypeA;
		public TerrainType transitionTypeB;
		public GameObject prefab;
		public GameObject cornerBigAPrefab;
		public GameObject cornerBigBPrefab;

		public bool IsTransitionForAB (TerrainType a, TerrainType b)
		{
			return (a == transitionTypeA && b == transitionTypeB) || (b == transitionTypeA && a == transitionTypeB);
		}
		
	}
}